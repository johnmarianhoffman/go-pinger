package main

import (
	"flag"
	pb "go-pinger/protocol/generated/pingspb"
)

var ping_request *pb.Envelope = &pb.Envelope{
	PingOneof: &pb.Envelope_PingRequest{
		PingRequest: &pb.PingRequest{},
	},
}

var ping_response *pb.Envelope = &pb.Envelope{
	PingOneof: &pb.Envelope_PingResponse{
		PingResponse: &pb.PingResponse{},
	},
}

func main() {

	address := ""
	server_mode := false

	flag.BoolVar(&server_mode, "s", server_mode, "Run a go-pinger server (instead of client)")
	flag.StringVar(&address, "a", address, "TCP address to connect to, or listen on")
	flag.Parse()

	if server_mode {
		server(address)
	} else {
		client(address)
	}

}