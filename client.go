package main

import (
	"net"
	pb "go-pinger/protocol/generated/pingspb"
	log "github.com/sirupsen/logrus"
	"time"
)


func client(address string) {
	log.Infof("Client address: %v", address)

	// Connect to the server
	resolved_address, err := net.ResolveTCPAddr("tcp4", address)
	if err != nil {
		log.Error("tcp resolution failed: %v", err)
		return
	}

	conn, err := net.Dial("tcp4", resolved_address.String())
	if err != nil {
		log.Error("tcp dial failed: %v", err)
		return
	}

	for {
		select {
		case <- time.After(1 * time.Second):
			start := time.Now()

			// Send the request
			if err := writeAMessage(conn, ping_request); err != nil {
				log.Error("send failed: %v", err)
				return
			}

			// Wait for the response
			msg, err := readAMessage(conn);
			if err != nil {
				log.Error("read failed: %v", err)
				return
			}

			switch msg.PingOneof.(type) {
			case *pb.Envelope_PingResponse:
				log.Infof("Ping time: %v", time.Since(start))
			}
		}
	}
}