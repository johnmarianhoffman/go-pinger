package main

import (
	"net"
	pb "go-pinger/protocol/generated/pingspb"
	log "github.com/sirupsen/logrus"
)

func client_handler(c net.Conn) {
	defer c.Close()

	for {
		msg, err := readAMessage(c)
		if err != nil {
			log.Errorf("could not read message: %v", err)
			return
		}

		switch msg.PingOneof.(type) {
		case *pb.Envelope_PingRequest:

			if err := writeAMessage(c, ping_response); err!=nil {
				log.Errorf("could not send response: %v", err)
				return
			}

		default:
			log.Errorf("receive unrecognized message type")
			return
		}
	}
}

func server(address string) {
	log.Infof("Server address: %v", address)

	resolved_address, err := net.ResolveTCPAddr("tcp4", address)
	if err != nil {
		log.Fatal("could not resolve tcp address for listening")
	}

	listener, err := net.ListenTCP("tcp", resolved_address)
	if err != nil {
		log.Fatal("could not listen on tcp:", err)
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Error("could not accept incoming connection:", err)
			return
		} else {
			log.Infof("new client connected: %v", conn.RemoteAddr())
		}

		go client_handler(conn)
	}

}
