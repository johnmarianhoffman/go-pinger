module go-pinger

go 1.17

require google.golang.org/protobuf v1.27.1

require (
	github.com/sirupsen/logrus v1.8.1 // indirect
	golang.org/x/sys v0.0.0-20191026070338-33540a1f6037 // indirect
)

replace go-pinger/protocol => ./protocol
