package main

import (
	"encoding/binary"
	"io"
	"errors"
	"net"
	pb "go-pinger/protocol/generated/pingspb"
	proto "google.golang.org/protobuf/proto"
	"fmt"
)

func readAMessage(rcv net.Conn) (*pb.Envelope, error) {
	msg_size_buffer := make([]byte, 2)

	n_bytes, err := io.ReadFull(rcv, msg_size_buffer)
	if err != nil || n_bytes != 2 {
		err_msg := fmt.Sprintf("reading message size failed: %v", err)
		return nil, errors.New(err_msg)
	}

	msg_size := int(binary.BigEndian.Uint16(msg_size_buffer[0:2]))

	msg_buff := make([]byte, msg_size)

	n_bytes, err = io.ReadFull(rcv, msg_buff)
	if err != nil || n_bytes != msg_size {
		err_msg := fmt.Sprintf("tcp read failed: %v", err)
		return nil, errors.New(err_msg)
	}

	// unmarshal the incoming message
	envelope := &pb.Envelope{}
	err = proto.Unmarshal(msg_buff, envelope)
	if err != nil {
		err_msg := fmt.Sprintf("could not unmarshal incoming message")
		return nil, errors.New(err_msg)
	}

	return envelope, nil
}

func writeAMessage(snd net.Conn, envelope *pb.Envelope) error {
	data, err := proto.Marshal(envelope)
	if err != nil {
		return errors.New("message serialization failed")
	}

	// Send the message size
	msg_size := make([]byte, 2)
	binary.BigEndian.PutUint16(msg_size, uint16(len(data)))

	_, err = snd.Write(msg_size)
	if err != nil {
		err_msg := fmt.Sprintf("network write failed: %v", err)
		return errors.New(err_msg)
	}

	//  Send the message
	_, err = snd.Write(data)
	if err != nil {
		err_msg := fmt.Sprintf("network write failed: %v", err)
		return errors.New(err_msg)
	}

	return nil
}